# ProviderDirectory
# Build

#### With Angular CLI
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

#### Without Angular CLI
Run `npm install` to install dependencies.
Run `npm run build` to build the project.

## Run/Open app
In the `dist/provider-directory` folder open index.html in a browser

## Running unit tests
#### With Angular CLI
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#### Without Angular CLI
Run `npm run test`to execute the unit tests via [Karma](https://karma-runner.github.io).
