import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Provider} from "../../models/provider";
import {Sort} from "../../models/sort";
import {cloneDeep} from 'lodash';

@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.css']
})
export class ProviderListComponent implements OnInit, OnChanges {
  @Input() providers: Provider[] = [];
  @Output() removeProviders = new EventEmitter<Provider[]>();
  providersToRemove: Provider[] = []
  sortOptions: Sort[] = [];
  currentSort: string = "last_name";
  emptyProvider = new Provider("","","","","");
  ascending: boolean = false;


  ngOnInit(): void {
    const sortKeys = Object.keys(new Provider("","","","",""));
    this.sortOptions = sortKeys.map(key => {
      const readable = this.translateKeyToReadableValue(key);
      return new Sort(key, readable);
    })

    this.sortProviders();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.sortProviders();
  }

  translateKeyToReadableValue(key: string) {
    const splitKey = key.split("_");
    return splitKey.map(x => x.charAt(0).toUpperCase() + x.slice(1)).join(" ");
  }

  toggleProviderCheckbox(provider: Provider) {
    const index = this.providersToRemove.indexOf(provider);
    if (index === -1) {
      this.providersToRemove.push(provider);
    } else {
      this.providersToRemove.splice(index, 1);
    }
  }

  onRemove() {
    this.removeProviders.emit(this.providersToRemove);
    this.providersToRemove = [];
  }

  sortProviders() {
    if (this.providers.length <= 6) {
      this.providers = this.removeEmptyProviders();
    }

    this.providers = this.providers.sort((a: Provider, b: Provider) => {
      return a[this.currentSort].localeCompare(b[this.currentSort]);
    })

    if (this.ascending) {
      this.providers = this.providers.reverse()
    }

    this.addEmptyProviders()
  }

  changeSortOrder() {
    if (this.providers.length <= 6) {
      this.providers = this.removeEmptyProviders();
    }

    this.providers = this.providers.reverse();
    this.ascending = !this.ascending;

    this.addEmptyProviders();
  }

  shouldRemoveBeDisabled() {
    return this.providersToRemove.length === 0;
  }

  removeEmptyProviders() {
    return this.providers.filter(provider => provider != this.emptyProvider);
  }

  addEmptyProviders() {
    while (this.providers.length < 6) {
      this.providers.push(this.emptyProvider);
    }
  }
}
