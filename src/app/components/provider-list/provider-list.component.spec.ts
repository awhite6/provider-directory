import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { ProviderListComponent } from './provider-list.component';
import {Provider} from "../../models/provider";
import {cloneDeep} from 'lodash';

describe('ProviderListComponent', () => {
  const INDEX_NOT_FOUND = -1
  const EMPTY = 0
  const EMPTY_PROVIDER = new Provider("","","","","")
  const providerList = [new Provider("first", "first", "first", "", ""),
    new Provider("middle", "middle", "middle", "", ""),
    new Provider("last", "last", "last", "", "")]
  let component: ProviderListComponent;
  let fixture: ComponentFixture<ProviderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProviderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderListComponent);
    component = fixture.componentInstance;
    component.providers = cloneDeep(providerList)
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.providersToRemove.length).toEqual(EMPTY);
  });

  it('should call onRemove() when remove button is clicked', waitForAsync(() => {
    spyOn(component, "onRemove");
    component.providersToRemove = [new Provider("","","","","")];
    const removeButton = fixture.debugElement.nativeElement.querySelector('#remove-providers');
    removeButton.disabled = false;
    removeButton.click();

    expect(component.onRemove).toHaveBeenCalled();
  }))

  it('should emit removeProviders event with array of providers when onRemove is called', () => {
    spyOn(component.removeProviders, "emit");
    const providersToBeRemoved = [new Provider("test", "", "", "", ""),
      new Provider("", "test", "", "", "")];
    component.providersToRemove = providersToBeRemoved;

    component.onRemove();

    expect(component.removeProviders.emit).toHaveBeenCalledWith(providersToBeRemoved);
  })

  it('should add provider to list when toggleProviderCheckbox is called and provider is not in list', waitForAsync(() => {
    const provider = new Provider("", "", "", "", "");
    const startingLengthProvidersToRemoved = component.providersToRemove.length;
    expect(component.providersToRemove.length).toEqual(EMPTY);
    component.toggleProviderCheckbox(provider);
    expect(component.providersToRemove.length).toEqual(startingLengthProvidersToRemoved + 1);
    expect(component.providersToRemove.indexOf(provider)).not.toEqual(INDEX_NOT_FOUND);
  }))

  it('should remove provider from list when toggleProviderCheckbox is called and provider is in list', waitForAsync(() => {
    component.providersToRemove = [providerList[0], providerList[1], providerList[2]];
    const startingLengthProvidersToRemoved = component.providersToRemove.length;
    expect(component.providersToRemove.length).toEqual(3);

    component.toggleProviderCheckbox(providerList[0]);

    expect(component.providersToRemove.length).toEqual(startingLengthProvidersToRemoved - 1);
    expect(component.providersToRemove.indexOf(new Provider("first", "first", "first", "", ""))).toEqual(INDEX_NOT_FOUND);
  }))

  it('should reverse order of sortedProviders when changeSortOrder is called', () => {
    const originalOrder = [new Provider("first", "first", "first", "", ""),
       new Provider("middle", "middle", "middle", "", ""),
       new Provider("last", "last", "last", "", "")];

    const expectedOrder = [new Provider("last", "last", "last", "", ""),
       new Provider("middle", "middle", "middle", "", ""),
       new Provider("first", "first", "first", "", ""),
       EMPTY_PROVIDER, EMPTY_PROVIDER, EMPTY_PROVIDER];

    component.providers = cloneDeep(originalOrder);
    component.changeSortOrder();
    expect(component.providers).toEqual(expectedOrder);
  })

  it('should change order of providers based on currentSort when sortProviders is called', () => {
    const expectedOrder = [new Provider("first", "first", "first", "", ""),
      new Provider("last", "last", "last", "", ""),
      new Provider("middle", "middle", "middle", "", ""),
      EMPTY_PROVIDER, EMPTY_PROVIDER, EMPTY_PROVIDER];

    component.currentSort = "last_name";
    component.sortProviders();
    expect(component.providers).toEqual(expectedOrder);
  })

  it('should call sortProviders when sort by select box is changed', waitForAsync(() => {
    spyOn(component, "sortProviders");

    const sortSelect = fixture.debugElement.nativeElement.querySelector('#sort-by');
    sortSelect.dispatchEvent(new Event('change'));

    expect(component.sortProviders).toHaveBeenCalled();
  }))

  it('should call changeSortOrder when sort order select box is changed', waitForAsync(() => {
    spyOn(component, "changeSortOrder");

    const sortSelect = fixture.debugElement.nativeElement.querySelector('#sort-order');
    sortSelect.dispatchEvent(new Event('change'));

    expect(component.changeSortOrder).toHaveBeenCalled();
  }))

  it('should enable remove button if providersToRemove is empty via shouldRemoveBeDisabled', () => {
    expect(component.shouldRemoveBeDisabled()).toBeTrue();
    component.providersToRemove = [new Provider("", "", "", "", "")];
    expect(component.shouldRemoveBeDisabled()).toBeFalse();
  })

  it('should add empty providers to list if length is less than or equal to 6 when calling sortProviders', () => {
    const expectedProviderList = [new Provider("first", "first", "first", "", ""),
      new Provider("last", "last", "last", "", ""),
      new Provider("middle", "middle", "middle", "", ""),
      EMPTY_PROVIDER, EMPTY_PROVIDER, EMPTY_PROVIDER];

    component.sortProviders();
    expect(component.providers).toEqual(expectedProviderList)
  })

  it('should remove empty providers from the list when removeEmptyProviders is called', () => {
    component.providers = [component.emptyProvider, component.emptyProvider, new Provider("first", "first", "first", "", "")]
    expect(component.removeEmptyProviders().length).toEqual(1);
  })

  it('should add the lightblue background to even numbered providers', () => {
    const firstProvider = fixture.debugElement.nativeElement.querySelector('#provider-0');
    const secondProvider = fixture.debugElement.nativeElement.querySelector('#provider-1');

    expect(firstProvider.classList.contains('dark-background')).toBeFalse();
    expect(secondProvider.classList.contains('dark-background')).toBeTrue();
  })

  it('should call toggleProviderCheckbox when a providers checkbox is clicked', () => {
    spyOn(component, "toggleProviderCheckbox");
    const firstProvider = fixture.debugElement.nativeElement.querySelector('#provider-0-checkbox');
    firstProvider.click();
    expect(component.toggleProviderCheckbox).toHaveBeenCalledWith(component.providers[0]);
  })

  it('should add empty providers until the list is at a length of 6', () => {
    component.providers = cloneDeep(providerList);
    expect(component.providers.length).toEqual(3);
    component.addEmptyProviders();
    expect(component.providers.length).toEqual(6);
  })
});
