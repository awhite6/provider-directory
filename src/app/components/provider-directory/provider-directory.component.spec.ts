import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderDirectoryComponent } from './provider-directory.component';
import {AppComponent} from "../../app.component";
import {Provider} from "../../models/provider";

describe('ProviderDirectoryComponent', () => {
  let component: ProviderDirectoryComponent;
  let fixture: ComponentFixture<ProviderDirectoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProviderDirectoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderDirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add given provider to providers when addProvider is called', () => {
    const fixture = TestBed.createComponent(ProviderDirectoryComponent);
    const component = fixture.componentInstance;
    const originalProvidersLength = component.providerData.length;
    const newProvider = new Provider("test", "test", "test", "", "");

    component.addProvider(newProvider);
    expect(component.providerData.length).toEqual(originalProvidersLength + 1);
    expect(component.providerData[component.providerData.length - 1]).toEqual(newProvider);
  })

  it('should remove the provider at the given indexes when removeProvider is called', () => {
    const fixture = TestBed.createComponent(ProviderDirectoryComponent);
    const component = fixture.componentInstance;
    const firstProvider = new Provider("first", "","","","");
    const secondProvider = new Provider("second", "","","","");
    const thirdProvider = new Provider("third", "","","","");
    const fourthProvider = new Provider("fourth", "","","","");
    component.providerData = [firstProvider, secondProvider, thirdProvider, fourthProvider];
    const originalProvidersLength = component.providerData.length;
    const removedProviders = [firstProvider, secondProvider];
    const providersToRemove = [removedProviders[0], removedProviders[1]];

    component.removeProviders(providersToRemove);
    expect(component.providerData.length).toEqual(originalProvidersLength - providersToRemove.length);
    expect(component.providerData[0]).not.toEqual(removedProviders[0]);
    expect(component.providerData[1]).not.toEqual(removedProviders[1]);
  })
});
