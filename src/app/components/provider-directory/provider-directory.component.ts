import {Component, Input, OnInit} from '@angular/core';
import {Provider} from "../../models/provider";
import {cloneDeep} from "lodash";

@Component({
  selector: 'app-provider-directory',
  templateUrl: './provider-directory.component.html',
  styleUrls: ['./provider-directory.component.css']
})
export class ProviderDirectoryComponent {
  @Input() providerData: Provider[] = [];

  addProvider(provider: Provider) {
    this.providerData.push(provider);
    this.providerData = cloneDeep(this.providerData);
  }

  removeProviders(providers: Provider[]) {
    providers.forEach(provider => {
      const index = this.providerData.indexOf(provider);
      this.providerData.splice(index, 1);
    })
    this.providerData = cloneDeep(this.providerData);
  }

}
