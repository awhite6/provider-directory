import {Component, Input, OnInit} from '@angular/core';
import {Provider} from "../../models/provider";

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent {
  @Input() data: Provider = new Provider("", "", "", "", "");
}
