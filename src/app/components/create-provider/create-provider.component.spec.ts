import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import { CreateProviderComponent } from './create-provider.component';
import {FormsModule} from "@angular/forms";
import {Provider} from "../../models/provider";

describe('CreateProviderComponent', () => {
  let component: CreateProviderComponent;
  let fixture: ComponentFixture<CreateProviderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ CreateProviderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should change provider object when inputs on forms are changed", () => {
    expect(component.provider.last_name).toEqual("");
    expect(component.provider.first_name).toEqual("");
    expect(component.provider.email_address).toEqual("");
    expect(component.provider.specialty).toEqual("");
    expect(component.provider.practice_name).toEqual("");

    fixture.whenStable().then(() => {
      const lastName = fixture.debugElement.nativeElement.querySelector('#last-name');
      lastName.value = "last name";
      lastName.dispatchEvent(new Event('input'));

      const firstName = fixture.debugElement.nativeElement.querySelector('#first-name');
      firstName.value = "first name";
      firstName.dispatchEvent(new Event('input'));

      const emailAddress = fixture.debugElement.nativeElement.querySelector('#email-address');
      emailAddress.value = "email address";
      emailAddress.dispatchEvent(new Event('input'));

      const specialty = fixture.debugElement.nativeElement.querySelector('#specialty');
      specialty.value = "specialty";
      specialty.dispatchEvent(new Event('input'));

      const practiceName = fixture.debugElement.nativeElement.querySelector('#practice-name');
      practiceName.value = "practice name";
      practiceName.dispatchEvent(new Event('input'));

      expect(component.provider.last_name).toEqual("last name");
      expect(component.provider.first_name).toEqual("first name");
      expect(component.provider.email_address).toEqual("email address");
      expect(component.provider.specialty).toEqual("specialty");
      expect(component.provider.practice_name).toEqual("practice name");
    })
  })

  it('should call onSubmit() when submit button is clicked', waitForAsync(() => {
    spyOn(component, "onSubmit");
    fixture.whenStable().then(() => {
      const lastName = fixture.debugElement.nativeElement.querySelector('#last-name');
      lastName.value = "last name";
      lastName.dispatchEvent(new Event('input'));

      const firstName = fixture.debugElement.nativeElement.querySelector('#first-name');
      firstName.value = "first name";
      firstName.dispatchEvent(new Event('input'));

      const emailAddress = fixture.debugElement.nativeElement.querySelector('#email-address');
      emailAddress.value = "email address";
      emailAddress.dispatchEvent(new Event('input'));

      const submitButton = fixture.debugElement.nativeElement.querySelector('#submit-provider');
      submitButton.click();

      expect(component.onSubmit).toHaveBeenCalled();
    })
  }))

  it('should emit createProvider event with the correct provider and set provider fields to empty when onSubmit is called', () => {
    const expectedProvider = new Provider("last", "first", "email", "", "");
    const emptyProvider = new Provider("", "", "", "", "")
    component.provider = expectedProvider;
    spyOn(component.createProvider, "emit");
    component.onSubmit();
    expect(component.createProvider.emit).toHaveBeenCalledWith(expectedProvider);
    expect(component.provider).toEqual(emptyProvider);
  })

  it('should not emit createProvider event if first name or last name are not valid', () => {
    spyOn(component.createProvider, "emit");
    component.provider.first_name = "     ";
    component.onSubmit();
    expect(component.createProvider.emit).not.toHaveBeenCalled();

    component.provider.first_name = "test";
    component.provider.last_name = "    ";
    component.onSubmit();
    expect(component.createProvider.emit).not.toHaveBeenCalled();

    component.provider.last_name = "test";
    component.onSubmit();
    expect(component.createProvider.emit).toHaveBeenCalled();
  })

  it('should return true if isFieldInvalid is called on a field that has only spaces for a value', () => {
    component.provider.first_name = "     ";
    expect(component.isFieldInvalid("first_name")).toBeTrue();
  })

  it('should return false if isFieldInvalid is called on a field that is valid', () => {
    component.provider.first_name = "test";
    expect(component.isFieldInvalid("first_name")).toBeFalse();
  })
});
