import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Provider} from "../../models/provider";

@Component({
  selector: 'app-create-provider',
  templateUrl: './create-provider.component.html',
  styleUrls: ['./create-provider.component.css']
})
export class CreateProviderComponent{
  @Output() createProvider = new EventEmitter<Provider>();
  provider: Provider = new Provider("", "", "", "", "");

  onSubmit(): void {
    if (!this.isFieldInvalid('first_name') && !this.isFieldInvalid('last_name')) {
      this.createProvider.emit(this.provider)
      this.provider = new Provider("", "", "", "", "");
    }
  }

  isFieldInvalid(field: string): boolean {
    return this.provider[field].length > 0 && this.provider[field].trim().length === 0;
  }
}

