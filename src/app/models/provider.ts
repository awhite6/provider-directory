export class Provider implements ProviderI {
  constructor(
    public last_name: string,
    public first_name: string,
    public email_address: string,
    public specialty: string,
    public practice_name: string
  ) {}

  [index: string]: string;
}

interface ProviderI {
  [index: string]: string;
}
