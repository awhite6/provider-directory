import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {CreateProviderComponent} from "./components/create-provider/create-provider.component";
import {ProviderListComponent} from "./components/provider-list/provider-list.component";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {Provider} from "./models/provider";

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        CreateProviderComponent,
        ProviderListComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'provider-directory'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('provider-directory');
  });
});
