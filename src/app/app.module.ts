import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF, LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { ProviderListComponent } from './components/provider-list/provider-list.component';
import { CreateProviderComponent } from './components/create-provider/create-provider.component';
import { ProviderComponent } from './components/provider/provider.component';
import { ProviderDirectoryComponent } from './components/provider-directory/provider-directory.component';

@NgModule({
  declarations: [
    AppComponent,
    ProviderListComponent,
    CreateProviderComponent,
    ProviderComponent,
    ProviderDirectoryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
