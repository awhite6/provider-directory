import {Component, OnInit} from '@angular/core';
import * as data from "../assets/provider-data.json";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'provider-directory';
  data = data;

  ngOnInit(): void {
  }

}
